import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import Router from './router/router.js';
import { Store } from './store/store.js';
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)
Vue.use(Vuex);
Vue.use(Router);

const app = new Vue({
  el: '#app',
  router: Router,
  store: Store,
  render: h => h(App)
});
