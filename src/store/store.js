import Vue from 'vue';
import Vuex from 'vuex';
import Router from '../router/router.js';
import getters from './getters.js';
import mutations from './mutations.js';
import actions from './actions.js';
import helpers from '../helpers/helpers.js';

Vue.use(Vuex);

export const Store = new Vuex.Store({
  state: {
    nav: {
      personal: {
        organization: {
          name: 'Org Name',
          address: 'City, State'
        }
      },
      kalendaroo: {},
      news: {},
    },
    signup: {
      progression: {
        steps: 2,
        current: 1
      },
      steps: [
        {
          instruction: {
            heading:
              'Before entering information about your program or camp, you need to set up a <b>business account.</b>',
            note:
              'Note: You may operate a number of programs or camps from the same business account.'
          },
          inputs: [
            {
              type: 'text',
              lbl: 'Business Name',
              name: 'business-name',
              value: '',
              state: null,
              helper: "Enter your business's name"
            },
            {
              type: 'text',
              lbl: 'First Name',
              name: 'first-name',
              value: '',
              state: null,
              helper: 'Enter your first name'
            },
            {
              type: 'text',
              lbl: 'Last Name',
              name: 'last-name',
              value: '',
              state: null,
              helper: 'Make sure to input only letters'
            },
            {
              type: 'text',
              lbl: 'Email',
              name: 'email',
              value: '',
              state: null,
              helper: 'Enter a valid email address'
            },
            {
              type: 'text',
              lbl: 'Email Confirm',
              name: 'email-confirm',
              value: '',
              state: null,
              helper: 'Must match your Email'
            },
            {
              type: 'password',
              lbl: 'Password',
              name: 'password',
              value: '',
              state: null,
              helper: 'Password must be 9 characters or longer'
            },
            {
              type: 'password',
              lbl: 'Password Confirm',
              name: 'password-confirm',
              value: '',
              state: null,
              helper: 'Must match your Password'
            }
          ],
          btn: {
            text: 'Contact Info',
            iconString: 'button--icon-right fa fa-arrow-circle-right',
            action: () => {
              store.commit('incrementProgression', { flow: 'signup' });
            }
          },
          valid: false
        },
        {
          instruction: {
            heading:
              'This is the information we will use to contact you and <b>will not</b> be posted publicly on the website.',
            note:
              'Note: You will have the opportunity to enter publicly posted contact information at a later stage.'
          },
          inputs: [
            {
              type: 'text',
              lbl: 'Mailing Address Line 1',
              name: 'address-1',
              value: '',
              state: null,
              helper: 'Enter your mailing address (line 1)'
            },
            {
              type: 'text',
              lbl: 'Mailing Address Line 2',
              name: 'address-2',
              value: '',
              state: null,
              helper: 'Enter your mailing address (line 2)'
            },
            {
              type: 'text',
              lbl: 'City',
              name: 'city',
              value: '',
              state: null,
              helper: 'Enter your city of residence'
            },
            {
              type: 'text',
              lbl: 'State',
              name: 'state',
              value: '',
              state: null,
              helper: 'Enter your state of residence'
            },
            {
              type: 'text',
              lbl: 'Zip Code',
              name: 'zip-code',
              value: '',
              state: null,
              helper: 'Enter your zip code of residence'
            },
            {
              type: 'text',
              lbl: 'Phone Number',
              name: 'phone-number',
              value: '',
              state: null,
              helper: 'Enter a 10-digit phone number'
            }
          ],
          btn: {
            text: 'Finish',
            iconString: 'button--icon-right fa fa-arrow-circle-right',
            action: () => {
              console.log('try registering');
            }
          },
          valid: false
        }
      ]
    },
    addnewprogram: {
      steps: [
        {
          step: '<b>Add</b> New Program',
          instruction: {
            heading:
              "Programs should be entered separately if they meet at <span class='txt-lightmagenta'>different locations</span>, occur at <span class='txt-lightmagenta'>different times of the year</span>, have <span class='txt-lightmagenta'>different audiences</span>, or fill <span class='txt-lightmagenta'>different categories</span>.",
            note:
              'Note: You may operate one or dozens of programs.'
          }
        },
        {
          step: '<b>Adding</b> John Doe\'s New Program',
        },
        {
          step: '<b>Adding</b> John Doe\'s New Program',
        },
        {
          step: '<b>Adding</b> John Doe\'s New Program',
          instruction: {
            heading: "Enter your program's contact information.",
            note: 'Note: If your program does not have a physical address, <a href="#" class="txt-lightmagenta">click here to enter the areas you serve</a>.'
          },
        },
        {
          step: '<b>Adding</b> John Doe\'s New Program',
        }
      ]
    },
    premiumupsell: {
      progression: {
        current: 1,
        steps: 2
      },
      steps: [
        {
          step: '<b>Upgrade to Premium!',
          instruction: {
            heading: 'All program listings are free. However, if you would like to maximize the KidGooRoo platform to grow and enhance your business, become a Premium Provider for just $500/year.',
          },
          features: [
            {
              collapsed: false,
              name: 'Access to KidGooRoo Parents',
              description: 'Tens of thousands of parents visit KidGooRoo every month to research programs for their kids. Attract new customers to your business through this powerful platform.',
              verified: false,
              premium: true
            }
          ],
          btns: [
            {
              text: 'no thanks',
              iconString: 'button--icon-right fa fa-arrow-circle-o-right',
              action: () => {
                console.log('[INSERT FREE OPTION CODE HERE]');
              }
            },
            {
              text: 'yes! Grow!',
              iconString: 'button--icon-right fa fa-arrow-circle-o-right',
              action: () => {
                store.commit('incrementProgression', { flow: 'premiumupsell' });
              }
            }
          ],
          valid: true
        },
        {
          step: '<b>Premium Checkout</b> John Doe\'s Sweet New Program',
          instruction: {
            heading: 'Enter your payment information',
            note: '<a href="#" class="txt-lightmagenta">or Click here to checkout with Paypal.</a>'
          },
          inputs: [
            {
              type: 'text',
              lbl: 'Name',
              name: 'name',
              value: '',
              state: null,
              helper: "Enter the name on your card"
            },
            {
              type: 'text',
              lbl: 'Card Number',
              name: 'card-number',
              value: '',
              state: null,
              helper: "Enter your card's number"
            },
            {
              type: 'text',
              lbl: 'Year',
              name: 'year',
              value: '',
              state: null,
              helper: "Enter your card's year"
            },
            {
              type: 'text',
              lbl: 'CVC',
              name: 'cvc',
              value: '',
              state: null,
              helper: "Enter your card's CVC number"
            },
            {
              type: 'text',
              lbl: 'Address Line 1',
              name: 'address-one',
              value: '',
              state: null,
              helper: "Enter the line 1 of your address"
            },
            {
              type: 'text',
              lbl: 'Address Line 1',
              name: 'address-two',
              value: '',
              state: null,
              helper: "Enter the line 2 of your address"
            },
            {
              type: 'text',
              lbl: 'City',
              name: 'city',
              value: '',
              state: null,
              helper: "Enter your city"
            },
            {
              type: 'text',
              lbl: 'State',
              name: 'state',
              value: '',
              state: null,
              helper: "Enter your state"
            },
            {
              type: 'text',
              lbl: 'Zip Code',
              name: 'zipcode',
              value: '',
              state: null,
              helper: "Enter your zipcode"
            }
          ],
          btns: [
            {
              text: 'Preview Your Listing',
              iconString: 'button--icon-right fa fa-arrow-circle-o-right',
              action: () => {
                console.log('[INSERT CODE FOR PREVIEWING LISTING]');
              }
            },
            {
              text: 'Submit for approval',
              iconString: 'button--icon-right fa fa-arrow-circle-o-right',
              action: () => {
                console.log('[INSERT CODE FOR SUBMITTING STRIPE PAYMENT]');
              }
            }
          ],
          valid: false
        }
      ]
    },
    organization: {
      name: '<b>Teddy\'s</b>',
      address: 'Wonderfully, Gloomy',
      programs: {
        active: [],
        drafts: [],
        pending: [
          {
            id: 'c4c873d2-d1b5-481e-abee-9cd4a6bf73c3',
            program_status: 'pending',
            title: 'Betty\'s Cupcake Meetup',
            address: '1456 Blueberry Street, Icing, Cupcake 12034',
            description: 'We will meet every single day to talk about cupcakes, make cupcakes, eat cupcakes, and poop cupcakes. Open bathroom, only.',
            website: 'cupcakesdaily.com',
            phone: '231-456-6102',
            contact_name: 'Betty White',
            type: 'premium',
            choices: [
              {
                categories: [
                  {
                    name: 'boys',
                    selected: true,
                    options: [
                      {
                        lbl: '0 - 2',
                        name: '0-2',
                        checked: false
                      },
                      {
                        lbl: '3 - 5',
                        name: '3-5',
                        checked: false
                      },
                      {
                        lbl: '6 - 10',
                        name: '6-10',
                        checked: false
                      },
                      {
                        lbl: '11 - 14',
                        name: '11-14',
                        checked: false
                      },
                      {
                        lbl: '15+',
                        name: '15+',
                        checked: false
                      }
                    ]
                  },
                  {
                    name: 'girls',
                    selected: false,
                    options: [
                      {
                        lbl: '0 - 2',
                        name: '0-2',
                        checked: false
                      },
                      {
                        lbl: '3 - 5',
                        name: '3-5',
                        checked: false
                      },
                      {
                        lbl: '6 - 10',
                        name: '6-10',
                        checked: true
                      },
                      {
                        lbl: '11 - 14',
                        name: '11-14',
                        checked: false
                      },
                      {
                        lbl: '15+',
                        name: '15+',
                        checked: true
                      }
                    ]
                  }
                ]
              },
              {
                categories: [
                  {
                    name: 'sports',
                    selected: true,
                    options: [
                      {
                        lbl: 'Baseball',
                        name: 'baseball',
                        checked: false
                      },
                      {
                        lbl: 'Basketball',
                        name: 'basketball',
                        checked: false
                      },
                      {
                        lbl: 'Boxing',
                        name: 'boxing',
                        checked: false
                      },
                      {
                        lbl: 'Cheer',
                        name: 'cheer',
                        checked: false
                      },
                      {
                        lbl: 'Crew / Rowing',
                        name: 'crew-rowing',
                        checked: false
                      }
                    ]
                  },
                  {
                    name: 'arts',
                    selected: false,
                    options: [
                      {
                        lbl: 'Painting',
                        name: 'Painting',
                        checked: false
                      },
                      {
                        lbl: 'Chalk',
                        name: 'chalk',
                        checked: false
                      },
                      {
                        lbl: 'Finger Painting',
                        name: 'finger-painting',
                        checked: false
                      },
                      {
                        lbl: 'Modeling',
                        name: 'modeling',
                        checked: false
                      },
                      {
                        lbl: 'Clay',
                        name: 'clay',
                        checked: false
                      }
                    ]
                  },
                  {
                    name: 'classes',
                    selected: false,
                    options: [
                      {
                        lbl: 'Math',
                        name: 'math',
                        checked: true
                      },
                      {
                        lbl: 'Science',
                        name: 'science',
                        checked: true
                      },
                      {
                        lbl: 'English',
                        name: 'english',
                        checked: false
                      },
                      {
                        lbl: 'History',
                        name: 'history',
                        checked: false
                      },
                      {
                        lbl: 'Chemistry',
                        name: 'chemistry',
                        checked: false
                      }
                    ]
                  },
                  {
                    name: 'leisure',
                    selected: false,
                    options: [
                      {
                        lbl: 'Walking',
                        name: 'walking',
                        checked: false
                      },
                      {
                        lbl: 'Golfing',
                        name: 'golfing',
                        checked: false
                      },
                      {
                        lbl: 'Sleeping',
                        name: 'sleeping',
                        checked: false
                      },
                      {
                        lbl: 'Baking',
                        name: 'baking',
                        checked: false
                      },
                      {
                        lbl: 'Smoking',
                        name: 'smoking',
                        checked: false
                      }
                    ]
                  }
                ]
              }
            ],
            images: [],
            reviews: [
              {
                reviewee: {
                  img: {
                    url: '/public/assets/kraked/review-image.jpeg',
                    alt: 'Image of reviewee'
                  },
                  name: 'Reviewee Mghee, Sr.'
                },
                date: 'Dec 17, 2016',
                review: {
                  title: 'This is an example review title',
                  description: 'This is an example review description printed four times. This is an example review description printed four times. This is an example review description printed four times. This is an example review description printed four times.',
                  ratings: {
                    overall: 5,
                    facilities: 5,
                    teachers: 5,
                    skill: 'beginner',
                    who: 'Boys, 6-10'
                  }
                }
              }
            ],
          }
        ],
        closed: []
      },
      events: [],
      news: []
    },
    listings: [
      {
        title: 'Unclaimed Program Name',
        address: 'City, State'
      },
      {
        title: 'Unclaimed Program Name',
        address: 'City, State'
      },
      {
        title: 'Unclaimed Program Name',
        address: 'City, State'
      },
      {
        title: 'Unclaimed Program Name',
        address: 'City, State'
      },
      {
        title: 'Unclaimed Program Name',
        address: 'City, State'
      }
    ]
  },
  getters: {
    getOrganization: (state) => {
      return getters.getOrganization(state);
    },
    getListings: (state) => {
      return getters.getListings(state);
    },
    getDraftProgram: (state) => (id) => {
      return getters.getDraftProgram(state, id);
    },
    getProgramTitles: (state) => {
      return getters.getProgramTitles(state);
    },
    getNumofEventsNews: (state) => (title) => {
      return getters.getNumofEventsNews(state, title);
    }
  },
  mutations: {
    initData: (state, payload) => {
      mutations.initData(state, payload);
    },
    createNewProgram: (state, payload) => {
      mutations.createNewProgram(state, payload);
    },
    createOrUpdateEvent: (state, payload) => {
      mutations.createOrUpdateEvent(state, payload);
    },
    createOrUpdateNews: (state, payload) => {
      mutations.createOrUpdateNews(state, payload);
    },
    saveDraftProgram: (state, payload) => {
      mutations.saveDraftProgram(state, payload);
    },
    updateProgram: (state, payload) => {
      mutations.updateProgram(state, payload);
    },
    addImageToProgram: (state, payload) => {
      mutations.addImageToProgram(state, payload);
    }
  },
  actions: {
    // programAPI
    programAPI: (context) => {
      actions.programAPI(context);
    }
  }
});