exports.getDraftProgram = function(state, id) {
  return state.organization.programs.drafts.find(draft => draft.id === id);
}

exports.getOrganization = function(state) {
  return state.organization;
};

exports.getListings = function(state) {
  return state.listings;
};

exports.getProgramTitles = function(state) {
  const programNames = [];
  for (type in state.organization.programs) {
    state.organization.programs[type].forEach(program => programNames.push(program.title));
  }
  return programNames;
}

exports.getNumofEventsNews = function(state, title) {
  let events = 0;
  let news = 0;
  state.organization.events.forEach(event => {
    if (event.program === title) events++;
  });
  state.organization.news.forEach(item => {
    if (item.program === title) news++;
  });
  return {
    events,
    news
  };
}

