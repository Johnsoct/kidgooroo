exports.addImageToProgram = function (state, payload) {
  const indexOfProgramInQuestion = state.organization.programs[payload.program_status].map(program => program.title).indexOf(payload.title);
  console.log(state.organization.programs[payload.program_status][indexOfProgramInQuestion]);
  state.organization.programs[payload.program_status][indexOfProgramInQuestion].images.push(payload.img);
};

exports.createNewProgram = function(state, payload) {
  const newProgram = {};
  newProgram.id = payload.draftProgram.id;
  newProgram.program_status = "pending";
  newProgram.title = payload.draftProgram.title
  newProgram.address = `${payload.draftProgram.address}, ${payload.draftProgram.city}, ${payload.draftProgram.state} ${payload.draftProgram.zipcode}`;
  newProgram.description = payload.draftProgram.description;
  newProgram.website = payload.draftProgram.website;
  newProgram.phone = payload.draftProgram.phone;
  newProgram.contact_name = payload.draftProgram.contact_name;
  newProgram.program_type = 'premium';
  newProgram.choices = payload.draftProgram.choices;
  newProgram.reviews = [];
  // newProgram.kalendaroo = [];
  // newProgram.news = [];
  newProgram.images = [payload.draftProgram.image];
  console.log(newProgram);
  state.organization.programs.pending.push(newProgram);

  // delete old draft
  const indexOfOldDraft = state.organization.programs.drafts.map(draft => draft.id).indexOf(payload.draftProgram.id);
  state.organization.programs.drafts.splice(indexOfOldDraft, 1);
};

exports.createOrUpdateEvent = function(state, payload) {
  function convertTime(time) {
    console.log(time);
    const timeStrings = time.split(':');
    //   timeStrings.forEach(string => parseInt(string, 10));
    const hours = parseInt(timeStrings[0], 10);
    const minutes = parseInt(timeStrings[1], 10);
    const convertedTime = {
      hours: hours,
      minutes: minutes
    };
    return convertedTime;
  }

  function calculateAllDay(starttime, endtime) {
    console.log(starttime, endtime);
    const start = convertTime(starttime);
    const end = convertTime(endtime);
    if (end.hours - start.hours < 4) return false;
    return true;
  }

  function convertDate(dates) {
    const eventDates = [
      {
        value: dates[0].value
      },
      {
        value: dates[1].value
      }
    ];

    eventDates.forEach(date => {
      const dateStrings = date.value.split('-');
      date.year = dateStrings[0];
      date.month = dateStrings[1];
      date.day = dateStrings[2];
    });

    const monthArray = [
      {
        name: 'January',
        abv: 'Jan'
      },
      {
        name: 'February',
        abv: 'Feb'
      },
      { 
        name: 'March',
        abv: 'Mar'
      },
      {
        name: 'April',
        abv: 'Apr'
      },
      {
        name: 'May',
        abv: 'May'
      },
      {
        name: 'June',
        abv: 'June'
      },
      {
        name: 'July',
        abv: 'July'
      },
      {
        name: 'August',
        abv: 'Aug'
      },
      {
        name: 'September',
        abv: 'Sept'
      },
      {
        name: 'October',
        abv: 'Oct'
      },
      {
        name: 'November',
        abv: 'Nov'
      },
      {
        name: 'December',
        abv: 'Dev'
      }
    ];
    eventDates.forEach(date => {
      const formattedMonth = monthArray.filter((arrayMonth, index) => index + 1 == date.month)[0].abv;
      date.formattedDate = `${formattedMonth} ${date.day}, ${date.year}`;
    })
    return eventDates[0].formattedDate;
  }

  const inputs = payload.inputs;
  console.log(inputs);
  const newEvent = {};
  newEvent.program = inputs.filter(input => input.name === 'title')[0].value;

  newEvent.name = inputs.filter(input => input.name === 'eventname')[0].value;

  newEvent.startdate = inputs.filter(input => input.name === 'startdate')[0].value;

  newEvent.enddate = inputs.filter(input => input.name === 'enddate')[0].value;

  newEvent.starttime = inputs.filter(input => input.name === 'starttime')[0].value;

  newEvent.endtime = inputs.filter(input => input.name === 'endtime')[0].value;

  newEvent.description = inputs.filter(input => input.name === 'description')[0].value;

  newEvent.address = inputs.filter(input => input.name === 'address')[0].value;
  console.log(newEvent);

  newEvent.start = inputs.filter(input => input.name === 'startdate')[0].value + 'T' + inputs.filter(input => input.name === 'starttime')[0].value;

  newEvent.end = inputs.filter(input => input.name === 'enddate')[0].value + 'T' + inputs.filter(input => input.name === 'endtime')[0].value;

  newEvent.allDay = calculateAllDay(newEvent.starttime, newEvent.endtime);

  newEvent.date = convertDate(inputs.filter(input => input.name === 'startdate' || input.name === 'enddate'));

  if (payload.type === 'create') state.organization.events.push(newEvent);
  if (payload.type === 'update') {
    const indexOfOldEvent = state.organization.events.map(events => events.name).indexOf(newEvent.name);
    state.organization.events.splice(indexOfOldEvent, 1, newEvent);
  }
};

exports.createOrUpdateNews = function (state, payload) {
  const inputs = payload.inputs;
  const newUpdate = {};
  newUpdate.program = inputs.filter(input => input.name === 'title')[0].value;
  newUpdate.name = inputs.filter(input => input.name === 'name')[0].value;
  newUpdate.image = inputs.filter(input => input.name === 'image')[0].value;
  newUpdate.description = inputs.filter(input => input.name === 'description')[0].value;

  if (payload.type === 'create') state.organization.news.push(newUpdate);
  if (payload.type === 'update') {
    const indexOfOldNews = state.organization.news.map(item => item.name).indexOf(newUpdate.name);
    state.organization.news.splice(indexOfOldNews, 1, newUpdate);
  }
};

exports.initData = function(state, payload) {
  const res = payload.response;
  // set organization's program with response
  // for each obj in the response array, push it to it's respective program array in organization.programs
  res.forEach(program => {
    state.organization.programs[program.program_status].push(program);
  });
};

exports.saveDraftProgram = function(state, payload) {
  const existingDraftPOS = state.organization.programs.drafts.map(draft => draft.id).indexOf(payload.draftObj.id);

  if (existingDraftPOS > -1) {
    state.organization.programs.drafts[existingDraftPOS] = Object.assign({}, payload.draftObj);
  } else {
    state.organization.programs.drafts.push(payload.draftObj);
  }
};

exports.updateProgram = function(state, payload) {
  const id = payload.program.id;
  const updatedProgram = payload.program;
  const indexOfProgramInQuestion = state.organization.programs[payload.program.program_status].map(program => program.id).indexOf(id);
  console.log(indexOfProgramInQuestion);
  state.organization.programs[payload.program.program_status].splice(indexOfProgramInQuestion, 1, payload.program);
};


