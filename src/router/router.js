import Vue from 'vue';
import Router from 'vue-router';
import userDashboard from '../components/main-flows/user-dashboard.vue';
import addNewProgram from '../components/add-new-program/add-new-program.vue';
import newProgramOne from '../components/add-new-program/new-program-one';
import newProgramTwo from '../components/add-new-program/new-program-two';
import newProgramThree from '../components/add-new-program/new-program-three';
import newProgramFour from '../components/add-new-program/new-program-four';
import newProgramFive from '../components/add-new-program/new-program-five';
import editProgram from '../components/main-flows/edit-program.vue';
import editEvent from '../components/main-flows/edit-event.vue';
import editNews from '../components/main-flows/edit-news.vue';
import programPage from '../components/main-flows/program-page.vue';
import kalendaroo from '../components/main-flows/kalendaroo.vue';
import addNewEvent from '../components/main-flows/add-new-event.vue';
import addNewNews from '../components/main-flows/add-new-news.vue';
import news from '../components/main-flows/news.vue';
import placeholder from '../components/placeholder.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: userDashboard,
      props: {
        currentNavItem: 'active'
      }
    },
    {
      path: '/programs',
      redirect: { name: 'Active Programs'},
      name: 'Program Dashboard',
      component: userDashboard,
      props: {
        currentNavItem: 'active'
      }
    },
    {
      path: '/programs/active-programs',
      name: 'Active Programs',
      component: userDashboard,
      props: {
        currentNavItem: 'active'
      }
    },
    {
      path: '/programs/draft-programs',
      name: 'Draft Programs',
      component: userDashboard,
      props: {
        currentNavItem: 'drafts'
      }
    },
    {
      path: '/programs/pending-programs',
      name: 'Pending Programs',
      component: userDashboard,
      props: {
        currentNavItem: 'pending'
      }
    },
    {
      path: '/programs/closed-programs',
      name: 'Closed Programs',
      component: userDashboard,
      props: {
        currentNavItem: 'closed'
      }
    },
    {
      path: '/program/add-new-program',
      name: 'New Program S1/5',
      component: addNewProgram,
      props: {
        progression: {
          current: 1,
          steps: 5
        }
      },
      children: [
        {
          path: 'one',
          name: 'Program Step One',
          component: newProgramOne,
          props: true
        }
      ]
    },
    {
      path: '/program/add-new-program',
      name: 'New Program S2/5',
      component: addNewProgram,
      props: {
        progression: {
          current: 2,
          steps: 5
        }
      },
      children: [
        {
          path: 'two/:programID',
          name: 'Program Step Two',
          component: newProgramTwo,
          props: true
        }
      ]
    },
    {
      path: '/program/add-new-program',
      name: 'New Program S3/5',
      component: addNewProgram,
      props: {
        progression: {
          current: 3,
          steps: 5
        }
      },
      children: [
        {
          path: 'three',
          name: 'Program Step Three',
          component: newProgramThree,
          props: true
        }
      ]
    },
    {
      path: '/program/add-new-program',
      name: 'New Program S4/5',
      component: addNewProgram,
      props: {
        progression: {
          current: 4,
          steps: 5
        }
      },
      children: [
        {
          path: 'four',
          name: 'Program Step Four',
          component: newProgramFour,
          props: true
        }
      ]
    },
    {
      path: '/program/add-new-program',
      name: 'New Program S5/5',
      component: addNewProgram,
      props: {
        progression: {
          current: 5,
          steps: 5
        }
      },
      children: [
        {
          path: 'five',
          name: 'Program Step Five',
          component: newProgramFive,
          props: true
        }
      ]
    },
    {
      path: '/program/claim-program',
      name: 'Claim a Program',
      component: addNewProgram,
    },
    {
      path: '/program/:program',
      name: 'Program',
      component: programPage,
      props: true
    },
    {
      path: '/program/:program/edit',
      name: 'Edit Program',
      component: editProgram,
      props: true
    },
    {
      path: '/kalendaroo',
      name: 'Calendar',
      component: kalendaroo
    },
    {
      path: '/kalendaroo/add-new-event',
      name: 'Add New Event',
      component: addNewEvent,
      props: true
    },
    {
      path: '/kalendaroo/:event/edit',
      name: 'Edit Event',
      component: editEvent,
      props: true
    },
    {
      path: '/news',
      name: 'News',
      component: news
    },
    {
      path: '/news/add-new-news',
      name: 'Add New News',
      component: addNewNews,
      props: true
    },
    {
      path: '/news/:news/edit',
      name: 'Edit News',
      component: editNews,
      props: true
    },
    {
      path: '/marketing-tools',
      name: 'Marketing Tools',
      component: placeholder,
    },
    {
      path: '/user-guide',
      name: 'User Guide',
      component: placeholder,
    },
    {
      path: '/faqs',
      name: 'FAQs',
      component: placeholder,
    }
  ]
})